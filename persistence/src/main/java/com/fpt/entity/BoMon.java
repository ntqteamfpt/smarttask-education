package com.fpt.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "bo_mon")
public class BoMon implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ma_nganh")
    @Getter
    @Setter
    private String maNganh;

    @Getter
    @Setter
    @Column(name = "ten_nganh")
    private String tenNganh;

    @Getter
    @Setter
    @Column(name = "trang_thai")
    private String trangThai;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "ma_vien")
    private KhoaVien khoaVien;

    @Getter
    @Setter
    @OneToMany(mappedBy = "boMon", fetch = FetchType.EAGER)
    private Set<MonHoc> lstMonHoc;

    @Getter
    @Setter
    @OneToMany(mappedBy = "boMon", fetch = FetchType.EAGER)
    private Set<SinhVien> lstSinhVien;

    @Getter
    @Setter
    @OneToMany(mappedBy = "boMon", fetch = FetchType.EAGER)
    private Set<GiaoVien> lstGiaoVien;

}
