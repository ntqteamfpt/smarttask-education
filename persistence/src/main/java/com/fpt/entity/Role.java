package com.fpt.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "role")
public class Role implements Serializable {

    @Getter
    @Setter
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ma_quyen", nullable = false)
    @Getter
    @Setter
    private String maQuyen;

    @Getter
    @Setter
    @Column(name = "ten_quyen", nullable = false)
    private String tenQuyen;

    @Getter
    @Setter
    @ManyToMany(mappedBy = "roles")
    private Set<User> users;

    public Role() {
    }

}