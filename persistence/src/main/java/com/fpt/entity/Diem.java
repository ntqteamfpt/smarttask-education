package com.fpt.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "diem_sinhvien")
public class Diem {

    @Getter
    @Setter
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Getter
    @Setter
    private String id;

    @Column(name = "diem_ly_thuyet")
    @Getter
    @Setter
    private Double diemLyThuyet;

    @Column(name = "diem_thuc_hanh")
    @Getter
    @Setter
    private Double diemThucHanh;

    @Column(name = "diem_cuoi_ki")
    @Getter
    @Setter
    private Double diemCuoiKi;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "ma_sinh_vien")
    private SinhVien sinhVien;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "ma_lop")
    private LopHoc lopHoc;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "ma_giao_vien")
    private GiaoVien giaoVien;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "ma_mon_hoc")
    private MonHoc monHoc;

    @Override
    public boolean equals (Object object) {
        boolean result = false;
        if (object == null || object.getClass() != getClass()) {
            result = false;
        } else {
            Diem diem = (Diem) object;
            if (this.sinhVien.equals(diem.getSinhVien()) && this.lopHoc.equals(diem.getLopHoc())) {
                result = true;
            }

        }
        return result;
    }
}
