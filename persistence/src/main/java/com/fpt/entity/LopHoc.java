package com.fpt.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.Set;

/**
 * Posted from Sep 11, 2018, 10:18 AM
 *
 * @author Vi Quynh (vi.quynh.31598@gmail.com)
 */
@Entity
@Table(name = "lop_hoc")
public class LopHoc {

    @Getter
    @Setter
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ma_lop")
    @Getter
    @Setter
    private String maLop;

    @Column(name = "mo_ta")
    @Getter
    @Setter
    private String moTa;

    @Column(name = "trang_thai")
    @Getter
    @Setter
    private String trangThai;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "phong_hoc")
    private PhongHoc phongHoc;

    @Getter
    @Setter
    @Column(name = "ngay_hoc")
    private String ngayHoc;

    @Getter
    @Setter
    @Column(name = "ca_hoc")
    private String caHoc;

    @Getter
    @Setter
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_tao")
    private Date ngayTao;

    @Getter
    @Setter
    @Column(name = "ngay_bat_dau")
    private Date ngayBatDau;

    @Getter
    @Setter
    @Column(name = "ngay_ket_thuc")
    private Date ngayKetThuc;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "ma_giao_vien")
    private GiaoVien giaoVien;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "ma_mon_hoc")
    private MonHoc monHoc;

    @Getter
    @Setter
    @OneToMany(mappedBy = "lopHoc", fetch = FetchType.EAGER)
    private Set<ThongBao> lstThongBao;

    @Getter
    @Setter
    @OneToMany(mappedBy = "lopHoc", fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<BaiTap> lstBaiTap;

    @Getter
    @Setter
    @ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    @JoinTable(
            name = "lop_sinhvien",
            joinColumns = { @JoinColumn(name = "ma_lop") },
            inverseJoinColumns = { @JoinColumn(name = "ma_sinh_vien") }
    )
    Set<SinhVien> sinhViens;

    @Getter
    @Setter
    @OneToMany(mappedBy = "lopHoc", fetch = FetchType.EAGER)
    private Set<BaiDang> lstBaiDang;

    @Getter
    @Setter
    @OneToMany(mappedBy = "lopHoc", fetch = FetchType.EAGER)
    private Set<Diem> lstDiem;

    @Getter
    @Setter
    @OneToMany(mappedBy = "lopHoc", fetch = FetchType.EAGER)
    private Set<TaiLieu> lstTaiLieu;

    public LopHoc() {
    }

}
