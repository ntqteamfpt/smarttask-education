package com.fpt.entity;

import lombok.Getter;
import lombok.Setter;

public class ThongBaoSocket {

    @Getter
    @Setter
    private String content;

    @Getter
    @Setter
    private String sender;

    @Getter
    @Setter
    private String receiver;

    @Getter
    @Setter
    private String time;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private String id;

    @Getter
    @Setter
    private int ki;

    @Getter
    @Setter
    private String maLop;

    public ThongBaoSocket() {
    }
}