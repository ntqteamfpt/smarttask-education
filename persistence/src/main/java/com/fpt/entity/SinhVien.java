package com.fpt.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;
import java.util.Set;

/**
 * Posted from Sep 10, 2018, 7:41 PM
 *
 * @author Vi Quynh (vi.quynh.31598@gmail.com)
 */

@JsonIgnoreProperties({"lstBaiTap", "lstPheDuyet","nhoms","lopHocs","khoaVien","lstThongBao"})
@Entity
@Table(name = "sinh_vien")
public class SinhVien {

    @Getter
    @Setter
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ma_sinh_vien")
    @Getter
    @Setter
    private String maSinhVien;

    @Getter
    @Setter
    @Column(name = "ngay_nhap_hoc")
    private Date ngayNhapHoc;

    @Getter
    @Setter
    @OneToMany(mappedBy = "sinhVien", fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<BaiTap> lstBaiTap;

    @Getter
    @Setter
    @ManyToMany(mappedBy = "sinhViens", fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<LopHoc> lopHocs;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "ma_vien")
    @JsonIgnore
    private KhoaVien khoaVien;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "ma_nganh")
    private  BoMon boMon;

    @Getter
    @Setter
    @OneToOne
    @JoinColumn(name="user_name")
    private User user;

    @Getter
    @Setter
    @OneToMany(mappedBy = "sinhVien", fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<ThongBao> lstThongBao;

    @Getter
    @Setter
    @OneToMany(mappedBy = "sinhVien", fetch = FetchType.EAGER)
    private Set<Diem> lstDiem;


    public SinhVien() {
    }

    @Override
    public boolean equals (Object object) {
        boolean result = false;
        if (object == null || object.getClass() != getClass()) {
            result = false;
        } else {
            SinhVien sinhVien = (SinhVien) object;
            if (this.maSinhVien.equals(sinhVien.getMaSinhVien())  ) {
                result = true;
            }
        }
        return result;
    }
}
