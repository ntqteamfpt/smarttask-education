package com.fpt.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "mon_hoc")
public class MonHoc {

    @Getter
    @Setter
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ma_mon_hoc")
    @Getter
    @Setter
    private String maMonHoc;

    @Getter
    @Setter
    @Column(name = "ten_mon_hoc")
    private String tenMonHoc;

    @Getter
    @Setter
    @Column(name = "trang_thai")
    private String trangThai;

    @Getter
    @Setter
    @Column(name = "tin_chi")
    private int tinChi;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "ma_nganh")
    private BoMon boMon;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "ma_ki" )
    private KiHoc kiHoc;

    @Getter
    @Setter
    @OneToMany(mappedBy = "monHoc", fetch = FetchType.EAGER)
    private Set<LopHoc> lstLopHoc;

    public MonHoc() {
    }
}
