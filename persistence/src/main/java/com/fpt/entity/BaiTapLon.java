package com.fpt.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "bai_tap_lon")
public class BaiTapLon implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @Getter
    @Setter
    private Integer id;

    @Column(name = "file_name")
    @Getter
    @Setter
    private String fileName;

    @Column(name = "file_real_name")
    @Getter
    @Setter
    private String fileRealName;

    @Column(name = "bai_tap")
    @Getter
    @Setter
    private String baiTap;

    @Column(name = "noi_dung")
    @Getter
    @Setter
    private String noiDung;

    @Column(name = "ngay_bat_dau")
    @Getter
    @Setter
    private Date ngayBatDau;

    @Column(name = "han_nop")
    @Getter
    @Setter
    private Date ngayKetThuc;

    @Column(name = "status")
    @Getter
    @Setter
    private String status;

    @ManyToOne
    @JoinColumn(name = "ma_giao_vien")
    @Getter
    @Setter
    private GiaoVien giaoVien;

    @ManyToOne
    @JoinColumn(name = "lop_hoc")
    @Getter
    @Setter
    private LopHoc lopHoc;

    public BaiTapLon() {
    }

}
