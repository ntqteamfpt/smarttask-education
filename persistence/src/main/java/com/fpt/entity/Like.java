package com.fpt.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "likes")
public class Like  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "likeid")
    @Getter
    @Setter
    private Integer likeId;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "postid")
    private BaiDang baiDang;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "user_name")
    private User user;

}
