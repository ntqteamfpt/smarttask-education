package com.fpt.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

/**
 * Posted from Sep 20, 2018, 10:16 PM
 *
 * @author Vi Quynh (vi.quynh.31598@gmail.com)
 */
@Entity
@Table(name = "ki_hoc")
public class KiHoc {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @Getter
    @Setter
    private int id;

    @Column(name = "ki")
    @Getter
    @Setter
    private String kiHoc;

    @Getter
    @Setter
    @OneToMany(mappedBy = "kiHoc", fetch = FetchType.EAGER)
    private Set<MonHoc> lstMonHoc;

    public KiHoc() {
    }
}
