package com.fpt.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "binh_luan")
public class BinhLuan {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "commentid")
    @Getter
    @Setter
    private Integer commentId;

    @Getter
    @Setter
    private String content;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date time;

    @ManyToOne
    @JoinColumn(name = "postid")
    @Getter
    @Setter
    private BaiDang baiDang;

    @JoinColumn(name = "user_name")
    @ManyToOne
    @Getter
    @Setter
    private User user;

}
