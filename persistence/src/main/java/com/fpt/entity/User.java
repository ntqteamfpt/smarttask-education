package com.fpt.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "user")
public class User implements Serializable {

    @Getter
    @Setter
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "user_name", nullable = false, unique = true)
    @Getter
    @Setter
    private String userName;


    @Getter
    @Setter
    @Column(name = "user_password", nullable = false)
    private String userPassWord;


    @Getter
    @Setter
    @Column(name = "full_name")
    private String fullName;


    @Getter
    @Setter
    @Column(name = "user_email")
    private String userEmail;


    @Getter
    @Setter
    @Column(name = "user_phone")
    private String userPhone;


    @Getter
    @Setter
    @Column(name = "user_avatar")
    private String userAvatar;


    @Getter
    @Setter
    @Column(name = "user_address")
    private String userAddress;

    @Getter
    @Setter
    @Column(name = "user_gender")
    private String userGender;

    @Getter
    @Setter
    @Column(name = "user_dob")
    private Date userDOB;


    @Getter
    @Setter
    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private GiaoVien giaoVien;


    @Getter
    @Setter
    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private SinhVien sinhVien;


    @Getter
    @Setter
    @ManyToMany(cascade = { CascadeType.ALL },fetch = FetchType.EAGER)
    @JoinTable(
            name = "phan_quyen",
            joinColumns = { @JoinColumn(name = "user_name") },
            inverseJoinColumns = { @JoinColumn(name = "ma_quyen") }
    )
    Set<Role> roles;


    @Getter
    @Setter
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private Set<BaiDang> lstBaiDang;


    @Getter
    @Setter
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private Set<BinhLuan> lstBinhLuan;


    @Getter
    @Setter
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private Set<Like> lstLike;

    public User() {
    }

    public User(String userName, String userPassWord, String fullName, String userEmail, String userPhone, String userAddress, String userGender, Date userDOB) {
        this.userName = userName;
        this.userPassWord = userPassWord;
        this.fullName = fullName;
        this.userEmail = userEmail;
        this.userPhone = userPhone;
        this.userAddress = userAddress;
        this.userGender = userGender;
        this.userDOB = userDOB;
    }

}