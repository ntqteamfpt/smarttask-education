package com.fpt.entity;

import lombok.Getter;
import lombok.Setter;

public class ThongKe {

    @Getter
    @Setter
    private String tenVien;

    @Getter
    @Setter
    private String tenNganh;

    @Getter
    @Setter
    private Integer slGiaoVien;

    @Getter
    @Setter
    private Integer slSinhVien;

    @Getter
    @Setter
    private Integer slPhongHoc;

    @Getter
    @Setter
    private Integer slTinChi;

    public ThongKe(String tenVien, String tenNganh, Integer slGiaoVien, Integer slSinhVien, Integer slPhongHoc, Integer slTinChi) {
        this.tenVien = tenVien;
        this.tenNganh = tenNganh;
        this.slGiaoVien = slGiaoVien;
        this.slSinhVien = slSinhVien;
        this.slPhongHoc = slPhongHoc;
        this.slTinChi = slTinChi;
    }

}
