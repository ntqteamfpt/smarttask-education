package com.fpt.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "bai_dang")
public class BaiDang implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "postid")
    @Getter
    @Setter
    private Integer postId;

    @Getter
    @Setter
    private String content;

    @Column(name = "file_name")
    @Getter
    @Setter
    private String fileName;

    @Column(name = "file_real_name")
    @Getter
    @Setter
    private String fileRealName;

    @Getter
    @Setter
    private Integer status;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date time;

    @OneToMany(mappedBy = "baiDang", fetch = FetchType.EAGER)
    @Getter
    @Setter
    private Set<BinhLuan> lstComment;

    @ManyToOne
    @JoinColumn(name = "user_name")
    @Getter
    @Setter
    private User user;

    @ManyToOne
    @JoinColumn(name = "lop_hoc")
    @Getter
    @Setter
    private LopHoc lopHoc;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "baiDang")
    @Getter
    @Setter
    private Set<Like> lstLike;

    @Transient
    @Getter
    @Setter
    private String imageSuffix;

    public BaiDang() {
    }
}
