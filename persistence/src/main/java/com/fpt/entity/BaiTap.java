package com.fpt.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Posted from Sep 11, 2018, 10:27 AM
 *
 * @author Vi Quynh (vi.quynh.31598@gmail.com)
 */
@Entity
@Table(name = "bai_tap")
public class BaiTap {

    @Getter
    @Setter
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @Getter
    @Setter
    private int id;

    @Column(name = "ngay_tao")
    @Getter
    @Setter
    private Date ngayTao;

    @Column(name = "file_name")
    @Getter
    @Setter
    private String fileName;

    @Column(name = "file_real_name")
    @Getter
    @Setter
    private String fileRealName;

    @ManyToOne
    @JoinColumn(name = "ma_sinh_vien")
    @Getter
    @Setter
    private SinhVien sinhVien;

    @ManyToOne
    @JoinColumn(name = "ma_lop")
    @Getter
    @Setter
    private LopHoc lopHoc;
}
