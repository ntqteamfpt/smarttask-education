package com.fpt.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "tai_lieu")
public class TaiLieu implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ma_tai_lieu")
    @Getter
    @Setter
    private Integer docId;

    @Getter
    @Setter
    @Column(name = "mo_ta")
    private String description;

    @Getter
    @Setter
    @Column(name = "file_name")
    private String fileName;

    @Getter
    @Setter
    @Column(name="file_real_name")
    private String fileRealName;

    @Getter
    @Setter
    private Integer status;

    @Getter
    @Setter
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date time;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "ma_lop")
    private LopHoc lopHoc;

}
