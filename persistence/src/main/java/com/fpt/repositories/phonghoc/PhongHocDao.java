package com.fpt.repositories.phonghoc;

import com.fpt.entity.PhongHoc;
import org.springframework.data.repository.CrudRepository;

public interface PhongHocDao extends CrudRepository<PhongHoc, Integer> {

}
